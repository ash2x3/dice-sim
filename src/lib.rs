#![crate_type = "lib"]

// :person_gesturing_no:: write a one line method
// :person_gesturing_ok:: write 30 lines to make a const generic zero sized type
#[allow(non_upper_case_globals)]
pub struct UniformDistribution<const Min: isize, const Max: isize>;

#[derive(PartialEq, Eq, Debug)]
pub enum HalvedInteger<T> {
    Exactly(T),
    HalfPlus(T),
}

#[allow(non_upper_case_globals)]
impl<const Min: isize, const Max: isize> UniformDistribution<Min, Max> {
    pub const fn expected_value() -> HalvedInteger<isize> {
        _expected_value(Min, Max)
    }
}
// extracted so we can test it
const fn _expected_value(min: isize, max: isize) -> HalvedInteger<isize> {
    let sum = max + min;
    let divided = sum / 2;
    if is_even(sum) {
        HalvedInteger::Exactly(divided)
    } else if sum > 0 {
        HalvedInteger::HalfPlus(divided)
    } else {
        HalvedInteger::HalfPlus(divided - 1)
    }
}
// todo: replace with a crate
const fn is_even(x: isize) -> bool {
    x & 1 == 0
}
#[cfg(test)]
mod tests {
    use proptest::prelude::*;
    use super::*;
    #[test]
    fn was_it_worth_it() {
        assert_eq!(std::mem::size_of::<UniformDistribution<0, 9>>(), 0);
    }

    proptest! {
        #[test]
        fn is_even_true_positives(x in -1000isize..1000isize) {
            prop_assert!(is_even(x * 2))
        }

        #[test]
        fn is_even_true_negatives(x in -1000isize..1000isize) {
            prop_assert!(!(is_even(1 + x * 2)))
        }

        // make sure that the integer shenanigans do what we think they do
        // especially wrt negative numbers.
        // TODO: move the match out by writing an Into<f32> impl for the struct
        #[test]
        fn expected_value_isnt_weird(x in -5000_isize..5000_isize, offset in 0_isize..10000_isize) {
            let y = x + offset;

            let correct_result = ((x as f32) + (y as f32)) / 2.0;

            let observed_result = match _expected_value(x, y) {
                HalvedInteger::Exactly(x) => x as f32,
                HalvedInteger::HalfPlus(x) => (x as f32) + 0.5,
            };
            let diff = (correct_result - observed_result).abs();
            prop_assert!(diff < f32::EPSILON);
        }
    }
}
